import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/cusmain',
    name: 'CusMain',
    component: () => import('../views/CusMain.vue')
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register.vue')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/menuCustomer',
    name: 'menuCustomer',
    component: () => import('../views/CusPage1.vue')
  },
  {
    path: '/formCustomer',
    name: 'formCustomer',
    component: () => import('../views/CusPage2.vue')
  },
  {
    path: '/queueCustomer',
    name: 'queueCustomer',
    component: () => import('../views/CusPage3.vue')
  },
  {
    path: '/com1',
    name: 'com1',
    component: () => import('../views/ComPage1.vue')
  },
  {
    path: '/com2',
    name: 'com2',
    component: () => import('../views/ComPage2.vue')
  },
  {
    path: '/Emp',
    name: 'Emp',
    component: () => import('../views/EmpPage1.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
